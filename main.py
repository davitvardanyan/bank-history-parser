import csv
import os
import sys
from datetime import datetime

import config


def formate_date(date_str: str) -> str:
    """
    Matching date string with predefined patterns and
    changing date format
    :param date_str: str
    :return: str
    """
    date_patterns = ['%b %d %Y', '%d %b %Y', '%d-%m-%Y']
    for pattern in date_patterns:
        try:
            return str(datetime.strptime(date_str, pattern).date())
        except ValueError:
            pass
    print(f"Date is not in expected format: {date_str}")
    sys.exit(0)


def format_amount(row: list, headers: list) -> list:
    """
    Modifying csv row, making amount from euro and cents
    :param row: list
    :param headers: list
    :return: list
    """
    euro_index = headers.index('euro')
    cents_index = headers.index('cents')
    row[euro_index] = f'{row[euro_index]}.{row[cents_index]}'
    row.pop(cents_index)
    return row


def generate_csv(data: list) -> None:
    """
    Generating csv file from nested list
    :param data: list
    :return:
    """
    if not os.path.exists(config.OUTPUT_DIRECTORY):
        os.makedirs(config.OUTPUT_DIRECTORY)
    with open(os.path.join(config.OUTPUT_DIRECTORY, f'{config.OUTPUT_FILE_NAME}.{config.OUTPUT_FILE_FORMAT}'), 'w') as out_csv:
        csvWriter = csv.writer(out_csv)
        csvWriter.writerows(data)


def generate_output(data: list) -> None:
    """
    Choosing output generator function based on OUTPUT_FILE_FORMAT in config
    :param data: nested list
    :return:
    """
    if config.OUTPUT_FILE_FORMAT == 'csv':
        generate_csv(data)


if __name__ == '__main__':
    new_headers = [['date', 'transaction', 'amount', 'to', 'from']]
    data = []
    for file_name in os.listdir(config.INPUT_DIRECTORY):
        if file_name.endswith('csv'):
            with open(os.path.join('data', file_name)) as csv_file:
                csvReader = csv.reader(csv_file, delimiter=',')
                headers = next(csvReader)
                for csv_row in csvReader:
                    if len(csv_row) == 0:
                        continue
                    elif ('euro' in headers) and ('cents' in headers):
                        csv_row = format_amount(csv_row, headers)
                    csv_row[0] = formate_date(csv_row[0])
                    data.append(csv_row)

    data = new_headers + sorted(data, key=lambda x: x[0])
    generate_output(data)
